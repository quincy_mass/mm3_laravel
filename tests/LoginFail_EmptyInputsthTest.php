<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginFail_EmptyInputsthTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testLoginFail_EmptyInputsthTest()
    {
        $this->visit('/')
             ->see('Password')
             ->see('Login')
             ->see('Username')
             ->see('Remember me')
             
             //empty username and password
             ->submitForm('Login', ['username' => '', 'password' => ''])
             ->see('Invalid Username or Password')
             
             //empty username but something in password
             ->submitForm('Login', ['username' => '', 'password' => 'abc'])
             ->see('Invalid Username or Password')
             
             //something in username but empty password
             ->submitForm('Login', ['username' => 'abc', 'password' => ''])
             ->see('Invalid Username or Password')
             
              //wrong username / password combo
             ->submitForm('Login', ['username' => 'abc', 'password' => 'abc'])
             ->see('Invalid Username or Password');
             
    }
}
