<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Login_SuccessfulsthTest extends TestCase
{
    /**
     * verify successful login
     */
    public function testLogin_SuccessfulsthTest()
    {
        $this->visit('/')
             ->see('Musicians Manager')
             ->see('Password')
             ->see('Login')
             ->see('Username')
             ->see('Remember me')
             ->see('Register')
             ->submitForm('Login', ['username' => 'pscheid', 'password' => 'Passw0rd'])
             ->see('Welcome to the Musicians\' Manager Website');
    }
}
